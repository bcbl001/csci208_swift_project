/*
 Ben Langley csci208
 Project Function and Operator Overloading
 Language: Swift
 
 
 swiftc overloading.swift
 ./overloading
 */

/*
 Swift allows for operator overloading, for example we may want to define the - function for vectors
 Modified from the + function for vectors here: https://www.raywenderlich.com/80818/operator-overloading-in-swift-tutorial
 Swift allows for overloading functions and will use the function which matches to the parameters used. For example when the float is used the function. Will not allow to overload with the same paramters, gives invalid redeclaration error
 */

/*
 Example of Operator overloading
 Overloading the - operator to work with vector subtraction
 Parameters vec1 = [x, y] and vec2 = [a, b]
 Return the value of vec1 - vec2 = [x - a, y - b]
 */
func -(vec1: [Int], vec2: [Int]) -> [Int] {
    var result: [Int] = []
    if(vec1.count != vec2.count){
        print("vectors must be the same length")
    } else {
        for (index, num) in vec1.enumerated() {
            result.append(num - vec2[index])
        }
    }
    return result
}

var vec1: [Int] = [9, 5]
var vec2: [Int] = [4, 1]
var subtraction: [Int] = vec1 - vec2

print(subtraction)

/*
 Creation of a new operator
 The operator 👆 makes any Integer given to it increase by one
 Parameter num is an Integer
 Return the number plus one
 */
func 👆(num: Int) -> Int {
    return num + 1
}

/*
 Overloading our new operator for floats
 The operator 👆 makes any Float given to it decrease by one
 Parameter num is a Float
 Return the number minus one
 */
func 👆(num: Float) -> Float {
    return num - 1
}

/*
 Overloading our new operator for floats again
 The operator 👆 makes any Float given to it increase by 1.2
 Parameter num is a Float
 Return the number plus 1.2
 */
//func 👆(num: Float) -> Float {
//    return num + 1.2
//}
//Gives error: invalid redeclaration

print(👆(num: 1))

print(👆(num: 2.3))



