/*
 Ben Langley csci208
 Project Exception Handling
 Language: Swift
 
 
 swiftc exceptions.swift
 ./exceptions
 */

/*
 Can throw errors or use a try catch system. The try catch system, actually called do-try-catch allows multiple catch statements but defer is similar to finally and allows for any clean multiple. You create your own exceptions using the enum type and Error protocol (an abstract class similar to interface in Java). 
 
 "You use a defer statement to execute a set of statements just before code execution leaves the current block of code. This statement lets you do any necessary cleanup that should be performed regardless of how execution leaves the current block of code—whether it leaves because an error was thrown or because of a statement such as return or break. For example, you can use a defer statement to ensure that file descriptors are closed and manually allocated memory is freed.
 
 A defer statement defers execution until the current scope is exited. This statement consists of the defer keyword and the statements to be executed later. The deferred statements may not contain any code that would transfer control out of the statements, such as a break or a return statement, or by throwing an error. Deferred actions are executed in reverse order of how they are specified—that is, the code in the first defer statement executes after code in the second, and so on."
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ErrorHandling.html
 */

//Throwing exceptions in a function
enum SampleError: Error{
    case WeirdError
    case SpookyError
}
func mayThroughError() throws -> Int {
    print("This function might throw an error so the keyword throws is necessary")
    throw SampleError.WeirdError
    return 0
}

//Do-Try-Catch
enum BankError: Error{
    case invalidAccount
    case insufficientFunds
}

func purchaseItem() throws{
    //Sample function that allows a person to buy an item at a store but may throw invalid account or insufficient funds error
}

do{
    try purchaseItem()
} catch BankError.invalidAccount {
    print("Your account information is invalid, please try again")
} catch BankError.insufficientFunds {
    print("You do not have enough money to complete the transaction")
}
