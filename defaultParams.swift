/*
 Ben Langley csci208
 Project Default Parameters
 Language: Swift
 
 
 swiftc defaultParams.swift
 ./defaultParams
 */

/*
 Swift allows default parameters by name: Type = value
 */
func defaultParams (x: Int, y: Int = 2, z: Int) -> Int{
    return x + y + z
}

print(defaultParams(x: 1, y: 3, z: 3)) //Override the deafult parameters
print(defaultParams(x: 1, z: 3)) //Using default y: 2


/*
 Swift allows multiple variables to be default
 */
func defaultParams2(x: Int = 1, y: Int = 2, z: Int) -> Int{
    return x + y + z
}

print(defaultParams2(z: 3))
