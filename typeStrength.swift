/*
 Ben Langley csci208
 Project Untyped, weakly typed, or strongly typed
 Language: Swift
 
 
 swiftc typeStrength.swift
 ./typeStrength
 */

/*
 Swift is strongly typed, it doesn't seem to do any implicit coercion besides Int -> Double
 */

print(3 + 4.0) //Prints 7.0
print(5.34/5) //Prints 1.068
print("5" + "37") //Prints 537
//print("hi" + 4) //error: binary operator '+' cannot be applied to operands of type 'String' and 'Int'
//print("w"/3) //error: binary operator '/' cannot be applied to operands of type 'String' and 'Int'
//print(true + 2) //error: binary operator '+' cannot be applied to operands of type 'Bool' and 'Int'
//print("w"/"h") //error: binary operator '/' cannot be applied to two 'String' operands
//print(false + true) //error: binary operator '+' cannot be applied to two 'Bool' operands


