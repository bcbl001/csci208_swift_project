/*
 Ben Langley csci208
 Project Strict vs. Nonstrict Eval
 Language: Swift
 
 
 swiftc eval.swift
 ./eval
 */

/*func foo() -> Bool{
    print("Nonstrict Eval") //This line is never run
    return false
}

if( 3 < 2 && foo()){
    print("Logical Operators broken")
} else {
    print("Strict Eval")
}*/


/*func bar(a: Double, b: Int) {
    print(b)
}

func divide(x: Double, y: Double) -> Double{
    return x / y
}

bar(a: divide(x: 1.0, y: 0.0), b: 5)
print(divide(x: 1.0, y: 0.0))
*/

//Prints dbl4 sqr4 36
/*
 Strict evaluation because dbl was evaluated before
 the result was required in the sqr function. This is an
 example of strict evaluation
 */

var a: Int = 3

func dbl(x: Int) -> Int{
    a = a + 1
    print("dbl" + String(a))
    return x + x
}

func sqr(x: Int) -> Int{
    print("sqr" + String(a))
    return x * x
}

print(sqr(x: dbl(x: a)))

