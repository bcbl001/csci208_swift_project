\documentclass[10pt]{extarticle}
\usepackage[utf8]{inputenc}
\usepackage[margin=1.1in]{geometry}
\usepackage{multicol, amsmath, url}
\usepackage{listings}

\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={Int(},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Java,                 % the language of the code
  morekeywords={func, var, let, import, true, false, init, fileprivative, static, private, internal, public, if, else, while, for, return, in, Int, Float, Double, String, Character, Bool},           % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\title{Swift Research Paper}
\author{Ben Langley}
\date{Fall 2016}

\begin{document}

\begin{titlepage}
    
    \vspace*{\fill}
        \begin{center}
            \Huge \textbf{Swift Research Paper}\\
            \LARGE \textbf{CSCI 208 Programming Languages}\\
            \Large By Ben Langley\\
            \Large Fall 2016
        \end{center}
    \vspace*{\fill}
\end{titlepage}

\section*{Introduction}
	The programming language Swift is an Object-Oriented, Imperative language. The structure of Swift is based on an ordered set of instructions. The language is also founded on the ability to create and manipulate objects. Follow these instructions to compile and run the Swift program in the file \textit{file.swift}\\
		\\
		Compile: swiftc file.swift\\
		Run: ./file
\section{Background}
	Development of Swift began in July 2010 and the first public release was unveiled on June 2nd, 2014. Although the project is now open source, the language was created by Apple Inc under the direction of Chris Lattner \footnote{\url{https://swift.org/} Swift Project Page}. Swift is a compiled language whose purpose is to be a fast and efficient way to build appliations for Apple's iOS operating system \footnote{\url{https://developer.apple.com/swift/} Swift Developer Page}. According to Apple, Swift is a “general-purpose programming language built using a modern approach to safety, performance, and software design patterns”$^1$. Since Swift is optimized for iOS development there are many jobs for mobile applications which require experience in Swift.
\section{Default Parameters}
	Swift allows for multiple default parameters and includes a system for overriding the defaults. Below is an example of a function with two default parameters with one function call using both default paramters and another call overriding the first default parameter.
	\begin{lstlisting}
		func defaultParams (x: Int = 1, y: Int = 2, z: Int) -> Int{
		    return x + y + z
		}
		print(defaultParams(z: 3)) //function call using default values for x and y
		print(defaultParams(x: 2, z: 3)) //function call overriding the default value for x
		/*
 		Output: 6  7
		*/
	\end{lstlisting}
\section{Static vs Dynamic Typing}
	Despite not requiring a type to be associated with the variable when declared, Swift is Statically typed. Once the type of a variable has been determined it will remain that type.
	\begin{lstlisting}
		var myVar = 9
		//myVar = "hello" //this line fails to compile
	\end{lstlisting}
	This code fails to compile on line 2 since the type of myVar is set to be an integer in line 1. This illustrates static typing.
\section{Implicit vs Explicit Typing}
	Swift has both implicit and explicit types although no matter how you declare the variable it is always statically typed. Below is an example of an integer variable which is first declared implicitly and then explicitly
	\begin{lstlisting}
		var myImplicitInt = 9
		var myExplicitInt: Int = 9
	\end{lstlisting}
	In general a variable can be declared explicitly as \textit{var name: Type}
\section{Nominal vs Structural Typing}
	Through the simple test below, it can be seen Swift is nominally typed. 
	\begin{lstlisting}
		class Person{
		    var name: String
		    
		    init(name: String){
		        self.name = name
		    }
		}

		class Dog{
		    var name: String
		    
		    init(name: String){
		        self.name = name
		    }
		}

		//var person: Person = Dog(name: "Ben") //Does not compile
	\end{lstlisting}
	Even though the two classes Person and Dog have the same structure they do not have the same name, and therefore Swift considers them different types. If Swift used structural typing then line 17 would compile since a Person and a Dog are structrually equivalent. However, line 17 fails to compile and therefore Swift uses nominal typing and not structural.
\section{Typing Strength}
	Swift falls on the strongly typed side of the typing spectrum. The language does not appear to do any implicit coercion besides Int to Double (more on coercion in the next section). Rather than providing funky answers to expressions such as “w"/3, Swift throws a compile time error.
	\begin{lstlisting}
		print(3 + 4.0) //Prints 7.0
		print(5.34/5) //Prints 1.068
		print("5" + "37") //Prints 537
		//print("hi" + 4) //error: binary operator '+' cannot be applied to operands of type 'String' and 'Int'
		//print("w"/3) //error: binary operator '/' cannot be applied to operands of type 'String' and 'Int'
		//print(true + 2) //error: binary operator '+' cannot be applied to operands of type 'Bool' and 'Int'
		//print("w"/"h") //error: binary operator '/' cannot be applied to two 'String' operands
		//print(false + true) //error: binary operator '+' cannot be applied to two 'Bool' operands
		/*
			Output: 7.0  1.068  537
		*/
	\end{lstlisting}
	Since most of the tests above fail, we conclude Swift is strongly typed.
\section{Coercion}
	Coercion is broken down into two categories, implicit and explicit. As discussed above (and shown in the code example above), Swift only implicitly coerces Int to Double. The expression “w" + “String" is also valid since String is defined as a list of Characters, and therefore I do not consider this coercion. Swift will not even implicitly coerce a number to a string. 
	\begin{lstlisting}
		//Implicit coercion: Int -> Double
		print(3 + 3.0) //6.0
		//print(3 + "String")
		print("w" + "String") //wString
		//print(1 + true)
	\end{lstlisting}
	Swift does allow more explicit coercion than it does implicit. As shown below, Strings can be coerced into Int and Double types. Using the String() constructor we can make Ints and Doubles into Strings. Using the Int() constrcutor, Swift allows for floats to be coerced into integers. 
	\begin{lstlisting}
		import Foundation

		//Explicit coercion
		var myStr = "3.0"
		print(String(3) + "Swift") //3Swift
		print((myStr as NSString).doubleValue) //3.0
		print(("350" as NSString).intValue) //350
		print(("3.0" as NSString).intValue) //3
		print(("3.5" as NSString).intValue) //3

		print(Int(true))
		//print(Int("w"))
		print(Int(3.5)) //3
		/*
			Output: 3Swift  3.0  350  3  3  1  3
		*/
	\end{lstlisting}
	It is interesting to note when coercing to an int from a value which has more information (ex: 3.5 or “3.5"), the coercion uses the floor function. So the result is 3 and not 4. The keyword \textbf{as} is used to downcast, cast to a subclass \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TypeCasting.html} Seift Documentation: Type Casting}.
\section{Static vs Dynamic Scope}
	The simple program below illustrates Swift uses static scope.
	\begin{lstlisting}
		var x = 5
		var y = 9

		func foo(){
		    var x = 2
		    x += 1 //The Swift compiler complains if the variable is unused, so here I am using it!
		    bar()
		}

		func bar(){
		    if (5 == x){
		        y = y + 1
		    }
		}

		foo()
		print(x) //Prints 5
		print(y) //Prints 10
		/*
			Output: 5  10
			Citation: Edited from Homework 6 problem 1 to work for Swift
		*/
	\end{lstlisting}
	Since the final value of y was 10 we know line 12 was executed, implying 5 == x evaluated to true. Hence the value of x was 5 and not 3. Since the globally declared x was used and not the x declared in foo(), we conclude Swift uses static scope.
\section{Overloading Function/Operators}
	Swift allows for both function and operator overloading. For example, we can define the - operator for vector subtraction
	\begin{lstlisting}
		/*
		 Example of Operator overloading
		 Overloading the - operator to work with vector subtraction
		 Parameters vec1 = [x, y] and vec2 = [a, b]
		 Return the value of vec1 - vec2 = [x - a, y - b]
		 */
		func -(vec1: [Int], vec2: [Int]) -> [Int] {
		    var result: [Int] = []
		    if(vec1.count != vec2.count){
		        print("vectors must be the same length")
		    } else {
		        for (index, num) in vec1.enumerated() {
		            result.append(num - vec2[index])
		        }
		    }
		    return result
		}

		var vec1: [Int] = [9, 5]
		var vec2: [Int] = [4, 1]
		var subtraction: [Int] = vec1 - vec2

		print(subtraction)
		/*
			Output: [5,4]
			Citation: Modified from the + function for vectors found here, https://www.raywenderlich.com/80818/operator-overloading-in-swift-tutorial
		*/
	\end{lstlisting}
	In addition to operator overloading Swift also allows for overloading functions and will use the function which matches to the parameters used. If the parameters for the overloaded functions are the same then the compiler will give an \textit{invalid redeclaration error}. Below is an example of an overloaded function
	\begin{lstlisting}
		/*
		 The function myFunction makes any Integer given to it increase by one
		 Parameter num is an Integer
		 Return the number plus one
		 */
		func myFunction(num: Int) -> Int {
		    return num + 1
		}

		/*
		 Overloading our new function for floats
		 The function myFunction makes any Float given to it decrease by one
		 Parameter num is a Float
		 Return the number minus one
		 */
		func myFunction(num: Float) -> Float {
		    return num - 1
		}

		/*
		 Overloading our new function for floats again
		 The function myFunction makes any Float given to it increase by 1.2
		 Parameter num is a Float
		 Return the number plus 1.2
		 */
		//func myFunction(num: Float) -> Float {
		//    return num + 1.2
		//}
		//Gives error: invalid redeclaration

		print(myFunction(num: 1))
		print(myFunction(num: 2.3))
		/*
			Output: 2  1.3
		*/
	\end{lstlisting}
	The function myFunction is overloaded, declared once with a single integer parameter, and second time with a float parameter. The output 2 1.3 shows the function call changed when the paramters were different. When attempting to define a third myFunction with the same parameters as the second an invalid redeclaration error was thrown at compile time.
\section{Strict vs Non-strict Evaluation}
	Swift uses strict evaluation. 
	\begin{lstlisting}
		var a: Int = 3

		func dbl(x: Int) -> Int{
		    a = a + 1
		    print("dbl" + String(a))
		    return x + x
		}

		func sqr(x: Int) -> Int{
		    print("sqr" + String(a))
		    return x * x
		}

		print(sqr(x: dbl(x: a)))
		/*
			Output: dbl4 sqr4 36
		*/
	\end{lstlisting}
	Since dbl was evaluated before the result was required in the sqr function, the above code illustrates strict evaluation.
\section{String Type}
	The String type in Swift is quite interesting. The built in String type is an array of Characters and is mutable. While String is native to Swift, the type NSString, used in Objective-C can still be used. By using the \textit{as} keyword Strings can be used as NSStrings, giving rise to many new properties to Strings in Swift. The only math operator which works on String is the + operator, which concatonates two strings. The mathematical comparisons such as ==, !=, >, <, >=, <= also work on the String type.
	\begin{lstlisting}
		var str: String = "Swift"
		print(str + str) //SwiftSwift
		print("Swift" < "Java") //false
		print(str == "Swift") //true
		print(str != "Swift") //false
		//print(str + 1)
		//print(str * 3)
		//print(str - str)
		//print(str - 2)
		/*
			Output: SwiftSwift  false  true  false
		*/
	\end{lstlisting}
	The String class includes methods and properties which allow for the manipulation of strings.
	\begin{lstlisting}
		var str: String = "Swift"
		print("\0") //
		print(str.isEmpty) //false
		print(str.uppercased()) //SWIFT
		print(str.lowercased()) //swift
		print(str.characters) //CharacterView(_core: Swift._StringCore(_baseAddress: Optional(0x000000010cf7e890), _countAndFlags: 5, _owner: nil))
		str.append("!"); print(str) //Swift!
		//var compare = str.caseInsensitiveCompare("swift")
		//print(compare)
		//"Swift".containsString("S")
		//print(str.contains("S"))

		str = "Swift"
		print(str.characters.count) //5
		print(str[str.startIndex]) //S
		//print(str.insert("fffff", at: 3))
		//print(str.remove(str.index(before: str.endIndex)))
		print(str.hasPrefix("Swi")) //true
		print(str.hasSuffix("ft")) //true
		var charUpTo = str.characters.index(of: "i")!; print(String(str.characters.prefix(upTo: charUpTo))) //Sw
		/*
			Output: see comments next to each line
			Citations: Used the following two sources to find string method functions and how to use them
					Swift Documentation: Strings and Characters https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/StringsAndCharacters.html
 					Swift String Cheat Sheet http://useyourloaf.com/blog/swift-string-cheat-sheet/
		*/
	\end{lstlisting}
	Finally, in Swift $\backslash$ 0 is the null character (as seen on line 2).
\section{Math Operators}
	 Since Swift is strongly typed, it does not allow any nonsense for its math operators. In fact, besides String (not even character) concatonation, math operators only work on numerical variables of the same type. An integer + a float will cause a compile time error (see section 7 on coercion). Despite the math operators not working, the comparison operators all work. This yields an interesting result since one string can be “less than" another string.
	 \begin{lstlisting}
	 	var myInt1: Int = 2
		var myInt2: Int = 4
		var myFloat1: Float = 2.0
		var myFloat2: Float = 4.0
		var myStr1: String = "hello"
		var myStr2: String = "world"
		var myChar1: Character = "d"
		var myChar2: Character = "w"

		//+, -, /, *, %, 
		//print(myInt1 + myFloat1)
		//print(myFloat2 - myInt2)
		print(myFloat2 / myFloat1) //2.0
		//print(myFloat2 / myInt2)

		print(myStr1 + myStr2) //helloworld
		//print(myStr2 - myStr1)
		//print(myStr1 / myStr2)
		//print(myStr2 * myStr1)
		//print(myStr2 * 2)
		//print(myStr1 % myStr2)

		//print(myChar2 + myChar1)
		//print(myChar1 - myChar2)
		//print(myChar2 * myChar1)
		//print(myChar1 * 3)
		//print(myChar1 / myChar2)
		//print(myChar2 % myChar1)

		//>, <, !=
		print(myInt2 < myInt1) //false
		print(myInt2 > myInt1) //true
		print(myInt2 != myInt1) //true

		print(myFloat2 < myFloat1) //false
		print(myFloat2 > myFloat1) //true
		print(myFloat2 != myFloat1) //true

		print(myStr2 < myStr1) //false
		print(myStr2 > myStr1) //true
		print(myStr2 != myStr1) //true

		print(myChar2 < myChar1) //false
		print(myChar2 > myChar1) //true
		print(myChar2 != myChar1) //true
		/*
			Output: see comments on each line
		*/
	 \end{lstlisting}
	 Although Swift does not allow math which does not make sense (see section 6 on typing stength), it does allow comparison which do not seem to make sense.
\section{Variables}
	There are multiple kinds of variables in Swift, fileprivate, internal, private, public, constant, class, and static. The default is internal, which allows the entity to be used within any source file in the module it is declared in. A module is a single unit of code distribution. Anything imported is a module. Open access, defined by the keyword \textbf{public}, is the least restrictive access control and allows the entity to be accessed in any source file which imports the module the entity is defined in. File private, which is more resitrive than internal, restricts access of the entity to only the source file in which it is defined. Finally, the most restricitve access control is private. The \textbf{private} keyword restricts access to the entity only in enclosing declaration \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/AccessControl.html} Swift Documentation: Access Control}.
	\par
	The constant variable is declared using the keyword \textbf{let}. Swift also allows for static variables using the keyword \textbf{static}.
	\begin{lstlisting}
		var myDefault: Int = 2
		fileprivate var myFilePrivateVar: Int = 1
		internal var myInternalVar: Int = 4 //default access control
		private var myPrivateVar: Int = 3
		public var myPublicVar: Int = 5
		let pi = 3.14159 //constant variable

		class Person {
		    var firstName: String //Class variables
		    static var lastName: String //let's assume all the people made are from the same family
		    var age: Int
		    init(firstName: String, age: Int){
		        self.firstName = firstName
		        self.age = age
		    }
		    init(firstName: String, lastName: String, age: Int){
		        self.firstName = firstName
		        self.lastName = lastName
		        self.age = age
		    }
		}
		/*
			Citations: Read the Swift documentation to understand the different types and how to declare them, then used that to create this example https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/AccessControl.html
		*/
	\end{lstlisting}
	The example above illustrates each variable.
\section{Parameter Evaluation Order}
	Using side effects and the fact Swift uses strict evaluation it is very easy determine the order of parameter evaluation.
	\begin{lstlisting}
		func order(func1: Any, func2: Any) {
		    print("Done")
		}

		order(func1: print("left"), func2: print("right"))
		/*
			Output: left  right  Done
		*/
	\end{lstlisting}
	Since the output of the program above is left right, the left parameter was evaluated first. Hence, the order of parameter evaluation is left to right.
\section{Short Circuit Evaluation}
	Swift uses short circuit evaluation in boolean operators such as $\& \&$ and $\mid \mid$ \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/BasicOperators.html} Swift Documentation: Basic Operators}. 
	\begin{lstlisting}
		func boolean() -> Bool {
		    print("Not Short Circuit Eval")
		    return true
		}

		if(false && boolean()){
		    print("Error!")
		} else {
		    print("Short Circuit Eval")
		}

		//Again prints only short circuit eval
		if(true || boolean()){
		    print("Short Circuit Eval")
		} else {
		    print("Error")
		}
		/*
			Output: Short Circuit Eval    Short Circuit Eval
		*/
	\end{lstlisting}
	The program only prints “Short Circuit Eval". This prooves Swift uses short circuit evaluation in boolean operators because the first expression is suffice to determine the final value so the second expression was never evaluated. If the second expression was evaluated we would've seen the side effect of printing “Not Short Circuit Eval".
\section{Control Structures}
	There are three basic control structures in Swift: if/else, loops, and transfers. If else statements allows for different blocks of code to run based on the evaluation of a specific expression. Swift also supports \textbf{switch}. The third option for if/else statements in Swift is the use of the trinary operator, condition ? execute this if true : execute this if false
	\begin{lstlisting}
		//If Else statement
		var myInt: Int = 5
		if myInt < 3 {
		    print("myInt is less than three")
		} else if myInt < 10 {
		    print("myInt is greater than or equal to three but less than ten")
		} else {
		    print("myInt is greather than or equal to ten")
		}

		//Trinary operator for if else
		myInt < 3 ? print("myInt is less than three") : print("myInt is greater than or equal to three")

		//Swicth Statement
		let myChar: Character = "a"
		switch myChar{
		    case "a":
		        print("Option a selected")
		    case "b":
		        print("Option b selected")
		    case "c":
		        print("Option c selected")
		    default:
		        print("Please select a valid option, a, b, or c")
		}
		/*
			Output: myInt is greater than or equal to three but less than ten
					myInt is greater than or equal to three
					Option a selected
		 */
	\end{lstlisting} 
	The next control structure is loops. There are three types of loops in Swift: for, while and repeat while. The for loops in Swift are actually for-in loops and iterate through a list. The while loop repeats a block of code until a condition is false. The repeat while loop is exactly the same as a while loop except the repeat while loop guarantees the code will execute atleast once.
	\begin{lstlisting}
		//For Loops
		for index in 1...10{
		    print(index) //print the numbers 1 through 10
		}

		let listNames = ["Ben", "Jordan", "Eric", "Eben"]
		for (index, value) in listNames.enumerated(){
		    print("The name at index \(index) is \(value)") //prints the index and name of each item in list
		}

		//While Loop
		var counter: Int = 1
		while counter <= 5{
		    print(counter) //print the numbers 1 through 5
		    counter += 1
		}

		//Repeat While Loop (Do-While Loop)
		repeat{
		    print("I will only be evaluated once!")
		} while false
		/*
			Output: 1 2 3 4 5 6 7 8 9 10
					The name at index 0 is Ben
					The name at index 1 is Jordan
					The name at index 2 is Eric
					The name at index 3 is Eben
					1 2 3 4 5
					I will only be evaluated once!
		 */
	\end{lstlisting}
	The final control structure are the control transfers. The control transfer statements change the order of execution by transferring control \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ControlFlow.html} Swift Documentation: Control Flow}.
	The control transfer statements are break, continue, fallthrough, return and throw. The break statement allows for the current control statement to end early. In most other languages the use of the keyword \textbf{break} is required to prevent other cases from executing. In Swift, the default is to break and the keyword \textbf{fallthrough} is required to fall down to the next case statement. The fallthrough statement allows allows each part of the switch statement to run. The continue statement directs the control structure to restart the loop immediately without exiting the loop as a break statement would do. Using the throw statement passes the control flow to your error handling (see section 17 on error handling).
	\begin{lstlisting}
		//Control Transfer (continue, break, fallthrough, return, throw)
		counter = 0
		while(true){
		    counter += 1
		    print(counter)
		    if(counter >= 3){
		        switch counter{
		            case 3:
		                print("The number is equal to 3")
		                fallthrough
		            default:
		                print("The number is greater than 0")
		        }
		        break
		    } else {
		        continue
		    }
		}
	\end{lstlisting}
\section{Error Handling}
	In Swift you can throw errors or use a try catch system. The try catch system, actually called do-try-catch allows multiple catch statements. The defer statement is similar to finally and allows for any clean up after the current scope is exited \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ErrorHandling.html} Swift Documentation: Error Handling}. 
	You create your own exceptions using the enum type and the Error protocol (an abstract class similar to interface in Java). An example is provided below.
	\begin{lstlisting}
		//Throwing exceptions in a function
		enum SampleError: Error{
		    case WeirdError
		    case SpookyError
		}
		func mayThroughError() throws -> Int {
		    print("This function might throw an error so the keyword throws is necessary")
		    throw SampleError.WeirdError
		    return 0
		}

		//Do-Try-Catch
		enum BankError: Error{
		    case invalidAccount
		    case insufficientFunds
		}

		func purchaseItem() throws{
		    //Sample function that allows a person to buy an item at a store but may throw invalid account or insufficient funds error
		}

		do{
		    try purchaseItem()
		} catch BankError.invalidAccount {
		    print("Your account information is invalid, please try again")
		} catch BankError.insufficientFunds {
		    print("You do not have enough money to complete the transaction")
		}
		/*
			Citations: Used the example of vending machine in the Swift Documentation and modified for a bank https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ErrorHandling.html
		*/
	\end{lstlisting}
\section{Static vs Dynamic Method Binding}
	Swift method binds very similar to Java. When calling a method on an instance of an object dynamic method binding is used. However when used in overloaded functions static method binding is used.
	\begin{lstlisting}
		class Cloud {
		    var name: String
		    var altitude: Int
		    
		    init(name: String, altitude: Int){
		        self.name = name
		        self.altitude = altitude
		    }
		    
		    func rain(){
		        print("It's raining!")
		        print("Static method binding")
		    }
		    
		    func toString() -> String{
		        return "The clouds name is \(name)"
		    }
		}

		class StormCloud: Cloud{
		    override init(name: String, altitude: Int){
		        super.init(name: name, altitude: altitude)
		    }
		    
		    override func rain(){
		        print("HUGE STORM!!")
		        print("Dynamic method binding")
		    }
		}

		func rain(cloud: Cloud){
		    print(cloud.toString())
		    print("Static method binding")
		}

		func rain(cloud: StormCloud){
		    print(cloud.toString())
		    print("Dynamic method binding")
		}

		let bob: Cloud = StormCloud(name: "Bob", altitude: 6000)
		bob.rain() //prints HUGE STORM!! Dyanmic method binding

		rain(cloud: bob) //prints The clouds name is Bob Static method binding
		/*
			Output: HUGE STORM!! Dynamic method binding
					the clouds name is Bob Static method binding
			Citation: I took the idea of storm and rain clouds from lecture
		*/
	\end{lstlisting}
	The Cloud named bob was initalized as a Storm Cloud. Since the rain method for Storm Cloud was executed, and not the method for Cloud, Swift used dynamic method binding in this case. The type of bob was not determined until run time. This is an example of calling a method on an instance of a class. On the other hand, the second part of the program above is determining method binding for overloaded functions. Both rain functions take a parameter called cloud, except the first function takes a Cloud and the second a Storm Cloud. Here, the function for Cloud is executed and not Storm Cloud. This shows static method binding for overloaded functions. It is also important to note that switching the order of the two rain functions does not matter, the output is always "the clouds name is Bob Static method binding"
\section{Constructors and Destructors}
	Constructors are handled using the init() method. The init() method can be overloaded but there is no default, the programmer must provide atleast one. Swift automatically deallocates instances, however manual deallocation is available with the deconstructor method deinit. The deinit method takes no parameters (therefore the developers of Swift decided to remove the paranthesis from the definition) and can only be defined once per class \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Deinitialization.html#//apple_ref/doc/uid/TP40014097-CH19-ID142} Swift Documentation: Deinitialization}. 
	\begin{lstlisting}
		class Person{
		    var name: String
		    var age: Int
		    var height: Int
		    init(name: String){
		        self.name = name
		        self.age = 0
		        self.height = 5
		    }
		    init(name: String, age: Int, height: Int){
		        self.name = name
		        self.age = age
		        self.height = height
		    }
		    func toString() -> String{
		        return "\(name) is \(age) years old and \(height) units tall"
		    }
		}

		let Ben = Person(name: "Ben", age: 19, height: 10)
		print(Ben.toString())
	\end{lstlisting}
	The above code illustrates the use of overloading the initializer for the Person class and creating an instance named Ben. Swift's automatic deallocation was used, however adding the deinit method to the Person class would allow for manual deallocation if desired.
\section{Anonymous Functions}
	Anonymous functions in Swift are called closure expressions (yes, expressions). The anonymous function for squaring a number is
	\begin{lstlisting}
		{
			(num: Int) -> Int in
			num * num
		}
		/*
			Citation: Found the format from the Swift Documentation https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Closures.html
		*/
	\end{lstlisting}
	Anonymous functions are used in higher order functions or as first class functions, so example of specific uses follow in the next two segments.
\section{Higher Order Functions}
	Map and Filter are both built into Swift and allow the use of anonymous functions. Implementing your own higher order functions is also permitted, such as the apply functions shown below. The apply function takes two parameters and applies the function parameter to the value parameter.
	\begin{lstlisting}
		//Example of Map using closure
		var ints: [Int] = [1,2,3,4,5,6,7,8,9,10]
		ints = ints.map({
		    (num: Int) -> Int in
		    num * num
		})
		print(ints) //[1,4,9,16,25,36,49,64,81,100]

		//Example of Filter using closure
		ints = [1,2,3,4,5,6,7,8,9,10]
		ints = ints.filter({
		    (num: Int) -> Bool in
		    num % 2 == 0
		})
		print(ints) //[2,4,6,8,10]

		//apply the function to value
		func apply(value: Int, function: (Int) -> Int) -> Int {
		    return function(value)
		}

		print(apply(value: 5, function: {(num: Int) -> Int in num + 1})) //6
		/*
			Output: [1,4,9,16,25,36,49,64,81,100]
					[2,4,6,8,10]
					6
		*/
	\end{lstlisting}
\section{First Class Functions}
	As allued to in section 20, anonymous functions are expressions. Hence, first class functions are allowed for anonymous function. However, named functions are not expressions in Swift and will result in a compile time error.
	\begin{lstlisting}
		let myFunc: (Int) -> Int = {(num: Int) -> Int in num + 1}
		//let myOtherFunc = func foo() {
		//    print("hey")
		//}
	\end{lstlisting}
\section{Statements vs Expressions and Functions vs Procedures}
	In Swift the following are statements and do not return a value: for-in loops, while loops, repeat while loops, if, switch, sontrol transfers (break, continue, fallthrough, return, throw), defer and do \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Statements.html#//apple_ref/doc/uid/TP40014097-CH33-ID428} Swift Documentation: Statements}. 
	The expressions which return a value are, try, binary operators, is, as, literals, self, closure expressions (anonymous function), parathenesized expressions, function calls and initializers \footnote{\url{https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Expressions.html#//apple_ref/doc/uid/TP40014097-CH32-ID383} Swift Documentation: Expressions}.
	\par
	Both procedures and functions in Swift are defined using the keyword \textbf{func}. To define a function, specify a return type in the declaration using “-$>$ returnType".
	\begin{lstlisting}
		\\Example of a function, returns a value
		func myFunction() -> Int {
			return 0
		}

		\\Example of a procedure, does not return a value
		func myProcedure() {
			print("This is a procedure!")
		}
	\end{lstlisting}
	Procedures and functions can be combined. The print command in line 8 could have been added to myFunction.
\end{document}







