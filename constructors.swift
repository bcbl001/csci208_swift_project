/*
 Ben Langley csci208
 Project Constructors
 Language: Swift
 
 
 swiftc constructors.swift
 ./constructors
 */

/*
 Constructors and handled using the init() function and can be overloaded. There is no defualt constructor. 
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Initialization.html#//apple_ref/doc/uid/TP40014097-CH18-ID203
 
 "Swift automatically deallocates your instances when they are no longer needed, to free up resources. Swift handles the memory management of instances through automatic reference counting (ARC), as described in Automatic Reference Counting. Typically you don’t need to perform manual cleanup when your instances are deallocated. However, when you are working with your own resources, you might need to perform some additional cleanup yourself. For example, if you create a custom class to open a file and write some data to it, you might need to close the file before the class instance is deallocated.
 
 Class definitions can have at most one deinitializer per class. The deinitializer does not take any parameters and is written without parentheses"
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Deinitialization.html#//apple_ref/doc/uid/TP40014097-CH19-ID142
 */

class Person{
    var name: String
    var age: Int
    var height: Int
    init(name: String){
        self.name = name
        self.age = 0
        self.height = 5
    }
    init(name: String, age: Int, height: Int){
        self.name = name
        self.age = age
        self.height = height
    }
    func toString() -> String{
        return "\(name) is \(age) years old and \(height) units tall"
    }
}

let Ben = Person(name: "Ben", age: 19, height: 10)
print(Ben.toString())
