/*
 Ben Langley csci208
 Project Nominal vs Structural Typing
 Language: Swift
 
 
 swiftc nominalStructural.swift
 ./nominalStructural
 */

/*
 Is typing determined by name or structure
 */

class Person{
    var name: String
    
    init(name: String){
        self.name = name
    }
}

class Dog{
    var name: String
    
    init(name: String){
        self.name = name
    }
}

//let person: Person = Dog(name: "Ben")
/*
 Even though structurally, Dog and Person are the same, they do not have the same name so therefore Swift will not allow this code. This proves Swift is nominally typed, does not use Duck Typing.
 */
