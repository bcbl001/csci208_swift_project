/*
 Ben Langley csci208
 Project Parameter Evaluation Order
 Language: Swift
 
 
 swiftc paramEvalOrder.swift
 ./paramEvalOrder
 */

/*
 The following program prints Left, Right, Done. Therefore the paramter evaulation order is left to right
 */

func order(func1: Any, func2: Any) {
    print("Done")
}

order(func1: print("left"), func2: print("right"))
