Why this is appropriate?
As discussed in my report, Swift was designed by Apple to be used for iOS application. Therefore, the best example of software in Swift is an application for iPhone.


How to use?
Open Xcode and select the magicCalculator project.
In the top right select iPhone 7 for the simulator
Next click the play button to build and run the project
The simulator will take a minute or two to open before the application 
will begin
From the main screen you can use the application as a four function basic calculator
Touching the top part of the black rectangle will open the hidden settings menu
	This is a little hard to find (as it is a secret menu) but clicking around the top box will eventually open the settings menu
From the settings menu you can type in the phone number, toggle between magic calculator and normal calculator, and learn more about magic calculator (as well as view a performance of the trick)

Magic Calculator was designed to look exactly like the stock iPhone calculator on the new iPhone 7. It can be used as a regular calculator and then easily toggled to the magicCalculator feature.