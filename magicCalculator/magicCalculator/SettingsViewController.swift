//
//  SettingsViewController.swift
//  magicCalculator
//
//  Created by Ben Langley on 11/30/16.
//  Copyright © 2016 Ben Langley. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController: UIViewController {
    
    let file = "settings.txt" //File to write data to

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //Set the initial values for the phone number text field and the switch
        (self.view.viewWithTag(100) as? UITextField)!.text = String(cellPhoneNumber)
        (self.view.viewWithTag(101) as? UISwitch)!.isOn = isMagic
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    @IBOutlet weak var magicSwitch: UISwitch!
    
    @IBAction func switchToggled(sender: UISwitch) {
        isMagic = magicSwitch.isOn
    }
    
    @IBAction func backButtonPressed(){
        //Get the main view
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "MainView")
        
        //Update the phone number
        //NOTE: isMagic is updated automatically in switchToggled action
        let phoneNumberStr: String = (self.view.viewWithTag(100) as? UITextField)!.text!
        cellPhoneNumber =  Int(phoneNumberStr)!
        
        //Show the main view again
        self.present(mainViewController, animated: false)
    }
    
}
