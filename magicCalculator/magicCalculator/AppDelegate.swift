//
//  AppDelegate.swift
//  magicCalculator
//
//  Created by Ben Langley on 11/28/16.
//  Copyright © 2016 Ben Langley. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        saveSettings()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        loadSettings()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        loadSettings()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        saveSettings()
    }
    
    /*
     Read the settings.txt file to get the user settings
     
     Citations: https://forums.developer.apple.com/thread/13049
     */
    func getSettings() -> String{
        do{
            let myFileURL = Bundle.main.url(forResource: "settings", withExtension: "txt")!
            let myText = try String(contentsOf: myFileURL, encoding: String.Encoding.utf8)
            return myText
        } catch {
            print("error")
        }
        return ""
    }
    
    /*
     Returns true is the string is "true" ignoring case, false otherwise
     */
    func toBool(string: String) -> Bool{
        if string.uppercased() == "TRUE" {
            return true
        } else {
            return false
        }
    }
    
    /*
     Returns "true" if bool is true and false otherwise
    */
    func boolToString(bool: Bool) -> String{
        if bool {
            return "true"
        } else {
            return "false"
        }
    }
    
    /*
     Handles loading user settings
    */
    func loadSettings(){
        //Load user settings
        let settings = getSettings()
        let charUpTo = settings.characters.index(of: " ")!
        let number = String(settings.characters.prefix(upTo: charUpTo))
        var bool = String(settings.characters.suffix(from: charUpTo))
        bool = bool.replacingOccurrences(of: " ", with: "")
        bool = bool.replacingOccurrences(of: "\n", with: "")
        print(number)
        print(bool)
        cellPhoneNumber = Int(number)!
        isMagic = toBool(string: bool)
    }
    
    /*
     Handles saving user settings
     */
    func saveSettings(){
        //Update settings file
        do{
            let myFileURL = Bundle.main.url(forResource: "settings", withExtension: "txt")!
            
            let isMagicStr = boolToString(bool: isMagic)
            let phoneNumberStr = String(cellPhoneNumber)
            let newSettings = phoneNumberStr + " " + isMagicStr
            print(newSettings)
            
            print(try "Current Document text: " + String(contentsOf: myFileURL))
            try newSettings.write(to: myFileURL, atomically: true, encoding: String.Encoding.utf8)
            print(try "New Document text: " + String(contentsOf: myFileURL))
            print(myFileURL.absoluteString)
        } catch let error {
            print(error)
        }
    }
}

