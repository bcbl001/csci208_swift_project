//
//  ViewController.swift
//  magicCalculator
//
//  Created by Ben Langley on 11/28/16.
//  Copyright © 2016 Ben Langley. All rights reserved.
//
//
//  UIAlertView modified from: http://stackoverflow.com/questions/25511945/swift-alert-view-ios8-with-ok-and-cancel-button-which-button-tapped
//
//

import UIKit

var cellPhoneNumber: Int = 0
var isMagic: Bool = false

class ViewController: UIViewController {
    
    var chosenFirstNumber = false
    var isTypingNumber = true
    var firstNumber: Double = 0
    var secondNumber: Double = 0
    var result: Double = 0
    var operation = 0
    var lastOperation = 0
    var firstNumberStr = ""
    var secondNumberStr = ""
    
    let file = "settings.txt" //File to read data in from

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Make the result label dyamically change font size
        let label: UILabel = (self.view.viewWithTag(100) as? UILabel)!
        label.adjustsFontSizeToFitWidth = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     Make the calculator errors
    */
    enum calculatorError: Error {
        case divideByZero
        case notANumber
    }
    
    /*
     Simple function which returns the numerical value of a string as a double
     
     Param value: String - the string to be coerced into an int
     Return - the double value of the parameter value
    */
    func getValue(value: String) -> Double {
        return Double(value)!
    }
    
    func cleanString(string: String) -> String {
        if(string.hasSuffix(".0")){
            let charUpTo = string.characters.index(of: ".")!
            return String(string.characters.prefix(upTo: charUpTo))
        } else {
            return string
        }
    }
    
    /*
     Update the text for the calculator screen
     
     Param updatedText: String - the new string to set as the label
    */
    func updateLabel(updatedText: String){
        //Clean the string first
        let cleanText = cleanString(string: updatedText)
        
        let label: UILabel = (self.view.viewWithTag(100) as? UILabel)!
        label.text = cleanText
    }
    
    //Returns x + y
    func add(x: Double, y: Double) -> Double {
        return x + y
    }
    
    //Returns x - y
    func sub(x: Double, y: Double) -> Double {
        return x - y
    }
    
    //Returns x * y
    func multi(x: Double, y: Double) -> Double{
        return x * y
    }
    
    //Returns x / y
    func div(x: Double, y: Double) -> Double{
        return x / y
    }
    
    //Returns x/100
    func percent(x: Double) -> Double{
        return x / 100
    }
    
    /*
     Reset the local variables to continue calculations
     
     Param num: Double - the new first number
    */
    func initialize(num: Double){
        firstNumber = num
        firstNumberStr = String(num)
        chosenFirstNumber = true
        isTypingNumber = true
        secondNumber = 0
        result = 0
        lastOperation = operation
        operation = 0
        secondNumberStr = ""
    }
    
    /*
     Reset the local variables to restart calculation
    */
    func initialize(){
        chosenFirstNumber = false
        isTypingNumber = true
        firstNumber = 0
        secondNumber = 0
        result = 0
        operation = 0
        firstNumberStr = ""
        secondNumberStr = ""
    }
    
    /*
     Find the correct operation to perform
     
     Return the result of the calculation
    */
    func chooseOperation() throws -> Double{
        switch operation {
        case 1: //Subtraction
            result = sub(x: firstNumber, y: secondNumber)
        case 2: //Addition
            result = add(x: firstNumber, y: secondNumber)
        case 3: //Multiplication
            result = multi(x: firstNumber, y: secondNumber)
        case 4: //Division
            //Check we are not dividing by zero
            if secondNumber == 0 {
                throw calculatorError.divideByZero
            } else {
                result = div(x: firstNumber, y: secondNumber)
            }
        default:
            operation = lastOperation
            return try chooseOperation()
        }
        return result
    }
    
    /*
     Do the entire calculation process and set up for the next calculation
    */
    func performOperation(){
        secondNumber = getValue(value: secondNumberStr)
        do{
            let result = try chooseOperation()
            updateLabel(updatedText: String(result))
            initialize(num: result)
        } catch calculatorError.divideByZero {
            //Alert Dialog
            let divideByZeroAlert = UIAlertController(title: "Divide by Zero", message: "Cannot divide by zero.", preferredStyle: UIAlertControllerStyle.alert)
            divideByZeroAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(action: UIAlertAction!) in
                self.initialize()
            }))
            present(divideByZeroAlert, animated: true, completion: nil)
        } catch{
            //Alert Dialog
            let divideByZeroAlert = UIAlertController(title: "Error", message: "Hmm..something didn't go right. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            divideByZeroAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(action: UIAlertAction!) in
                self.initialize()
            }))
            present(divideByZeroAlert, animated: true, completion: nil)
        }
    }
    
    @IBAction func settingsPressed(){
        //Switch views
        let storyBoard : UIStoryboard = UIStoryboard(name: "Settings", bundle:nil)
        let resultViewController = storyBoard.instantiateViewController(withIdentifier: "ResultView")
        self.present(resultViewController, animated:false)
    }
    
    @IBAction func clearPressed(){
        initialize()
        updateLabel(updatedText: "0")
    }
    
    @IBAction func plusMinusPressed() {
        if chosenFirstNumber && (secondNumberStr != "") {
            secondNumberStr = String("-") + secondNumberStr
            updateLabel(updatedText: secondNumberStr)
        } else if (firstNumberStr != "") {
            firstNumberStr = String("-") + firstNumberStr
            updateLabel(updatedText: firstNumberStr)
        }
    }
    
    @IBAction func percentPressed() {
        if (firstNumberStr != "") && (secondNumberStr == ""){ //First number selected but not second number
            firstNumber = getValue(value: firstNumberStr)
            firstNumber = percent(x: firstNumber)
            firstNumberStr = String(firstNumber)
            updateLabel(updatedText: firstNumberStr)
        }
    }

    @IBAction func numberPressed(button: UIButton) {
        let text = button.tag
        var textStr: String
        //Code for '.' is -1
        if text == -1 {
            textStr = "."
        } else {
            textStr = String(text)
        }
        if chosenFirstNumber { //keep adding to the second number
            secondNumberStr += textStr
            updateLabel(updatedText: secondNumberStr)
        } else { //keep adding to the first number
            firstNumberStr += textStr
            updateLabel(updatedText: firstNumberStr)
        }
    }
    
    @IBAction func operatorPressed(button: UIButton) {
        let value = button.tag
        if (firstNumberStr == "") {
            //Alert Dialog
            let noOperandAlert = UIAlertController(title: "Nothing to Compute", message: "Please input a number before using an operation.", preferredStyle: UIAlertControllerStyle.alert)
            noOperandAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(action: UIAlertAction!) in
                self.initialize()
            }))
            present(noOperandAlert, animated: true, completion: nil)
        } else if chosenFirstNumber && (firstNumberStr == "") {
            //show warning that they need to choose a first number
            //Alert Dialog
            let noOperandAlert = UIAlertController(title: "Nothing to Compute", message: "Please input a number before using an operation.", preferredStyle: UIAlertControllerStyle.alert)
            noOperandAlert.addAction(UIAlertAction(title: "Ok", style: .default))
            present(noOperandAlert, animated: true, completion: nil)
        } else if chosenFirstNumber && (secondNumberStr != "") { //Perform operation and then continue
            operation = value
            performOperation()
        } else { //change to second number
            chosenFirstNumber = true
            operation = value
            firstNumber = getValue(value: firstNumberStr) //get the integer value of the string
        }
    }
    
    @IBAction func equalsPressed(button: UIButton){
        if (firstNumberStr == "") || (secondNumberStr == ""){
            //Alert Dialog
            let noOperandsAlert = UIAlertController(title: "Invalid Input", message: "Please type a number before using the operation", preferredStyle: UIAlertControllerStyle.alert)
            noOperandsAlert.addAction(UIAlertAction(title: "Ok", style: .default))
            present(noOperandsAlert, animated: true, completion: nil)
        } else {
            performOperation()
            if(isMagic){
                updateLabel(updatedText: String(cellPhoneNumber))
            }
        }
    }
}

