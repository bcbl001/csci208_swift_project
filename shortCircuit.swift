/*
 Ben Langley csci208
 Project Short Circuit Evaluation
 Language: Swift
 
 
 swiftc shortCircuit.swift
 ./shortCircuit
 */

/*
 Program only prints "Short Circuit Eval". This prooves Swift uses short circuit eval in boolean operators because the first expression is suffice to determine the final value so the second expression is never evaluated. If the second expression was evaluated we would've seen the side effect of printing "Not Short Circuit Eval"
 Apple's Docuemntation suppports the fact: https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/BasicOperators.html
 */

func boolean() -> Bool {
    print("Not Short Circuit Eval")
    return true
}

if(false && boolean()){
    print("Error!")
} else {
    print("Short Circuit Eval")
}

//Again prints only short circuit eval
if(true || boolean()){
    print("Short Circuit Eval")
} else {
    print("Error")
}
