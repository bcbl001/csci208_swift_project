/*
 Ben Langley csci208
 Project Static vs. Dynamic Typing
 Language: Swift
 
 
 swiftc staticDynamicTyping.swift
 ./staticDynamicTyping
 */


/*
 Despite not requiring a type to be associated with the variable when declared,
 Swift is Statically typed. Once the type of a variable has been determined it 
 will remain that type.
 */
var myInt = 9 //At compile time the type of myInt is infered to be Int and then cannot be changed
//myInt = "Dynamic Typing!" //This line fails at compile time "error: cannot assign value of type 'String' to type 'Int' "

print(myInt)


//WTF?!?!
let 🐶🐮 = "dogcow"
print(🐶🐮)
