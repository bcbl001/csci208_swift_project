/*
 Ben Langley csci208
 Project Variables
 Language: Swift
 
 
 swiftc variables.swift
 ./variables
 */

/*
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/AccessControl.html
 "Open access and public access enable entities to be used within any source file from their defining module, and also in a source file from another module that imports the defining module. You typically use open or public access when specifying the public interface to a framework. The difference between open and public access is described below.
 Internal access enables entities to be used within any source file from their defining module, but not in any source file outside of that module. You typically use internal access when defining an app’s or a framework’s internal structure.
 File-private access restricts the use of an entity to its own defining source file. Use file-private access to hide the implementation details of a specific piece of functionality when those details are used within an entire file.
 Private access restricts the use of an entity to the enclosing declaration. Use private access to hide the implementation details of a specific piece of functionality when those details are used only within a single declaration.
 Open access is the highest (least restrictive) access level and private access is the lowest (most restrictive) access level.
 
 Classes with public access, or any more restrictive access level, can be subclassed only within the module where they’re defined.
 Class members with public access, or any more restrictive access level, can be overridden by subclasses only within the module where they’re defined.
 Open classes can be subclassed within the module where they’re defined, and within any module that imports the module where they’re defined.
 Open class members can be overridden by subclasses within the module where they’re defined, and within any module that imports the module where they’re defined."
 */

var myDefault: Int = 2
fileprivate var myFilePrivateVar: Int = 1
internal var myInternalVar: Int = 4 //default access control
private var myPrivateVar: Int = 3
public var myPublicVar: Int = 5
let pi = 3.14159 //constant variable

class Person {
    var firstName: String //Class variables
    static var lastName: String //let's assume all the people made are from the same family
    var age: Int
    init(firstName: String, age: Int){
        self.firstName = firstName
        self.age = age
    }
    init(firstName: String, lastName: String, age: Int){
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
    }
}
