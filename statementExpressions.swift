/*
 Ben Langley csci208
 Project Statements, Expressions, Functions and Procedures
 Language: Swift
 
 
 swiftc statementExpressions.swift
 ./statementExpressions
 */

/*
 Expressions return a value but statements do not
 Functions return a value but procedures do not
 
 Statements: https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Statements.html#//apple_ref/doc/uid/TP40014097-CH33-ID428
    For-In 
    While
    Repeat While
    If
    Switch
    Control Transfers (break, continue, fallthrough, return, throw)
    Defer
    Do
 
 Expressions: https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Expressions.html#//apple_ref/doc/uid/TP40014097-CH32-ID383
    Try
    Binary operators
    is, as, as? and as!
    Literals
    Self
    Closure Expressions (aka anonymous function)
    Parathenesized Expressions
    Function Calls
    Initializer
    
 Functions vs Procedures
    Only differentiated by the -> returmType in the function declaration
 */
