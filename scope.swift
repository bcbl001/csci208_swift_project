/*
 Ben Langley csci208
 Project Dynamic vs Static Scope
 Language: Swift
 
 
 swiftc scope.swift
 ./scope
 */

/*
 Swift is statically scoped -- code edited from Homework on scoping
 */

var x = 5
var y = 9

func foo(){
    var x = 2
    x += 1
    bar()
}

func bar(){
    if (5 == x){
        y = y + 1
    }
}


foo()
print(x) //Prints 5
print(y) //Prints 10







