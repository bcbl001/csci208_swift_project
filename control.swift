/*
 Ben Langley csci208
 Project Control Structures
 Language: Swift
 
 
 swiftc control.swift
 ./control
 */

/*
 Swift control structures include: If\Else, Function, For loops, While Loops (and repeat while), Switch statements, Control Transfer (continue, break, fallthrough, return, throw), defer
 
 Control transfer statements change the order in which your code is executed, by transferring control from one piece of code to another.
 
 The continue statement tells a loop to stop what it is doing and start again at the beginning of the next iteration through the loop. It says “I am done with the current loop iteration” without leaving the loop altogether.
 
 The break statement ends execution of an entire control flow statement immediately. The break statement can be used inside a switch statement or loop statement when you want to terminate the execution of the switch or loop statement earlier than would otherwise be the case.
 
If you need C-style fallthrough behavior, you can opt in to this behavior on a case-by-case basis with the fallthrough keyword.
    --> Fallthrough allows each part of the switch statement to run. In C we require the keyword break to prevent fallthrough whereas in swift the default is to break and we must use the keyword fallthrough if we wish to fall down to the next case in a switch statement
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ControlFlow.html
 
 You use a defer statement to execute a set of statements just before code execution leaves the current block of code. This statement lets you do any necessary cleanup that should be performed regardless of how execution leaves the current block of code—whether it leaves because an error was thrown or because of a statement such as return or break. For example, you can use a defer statement to ensure that file descriptors are closed and manually allocated memory is freed.
 
 A defer statement defers execution until the current scope is exited. This statement consists of the defer keyword and the statements to be executed later. The deferred statements may not contain any code that would transfer control out of the statements, such as a break or a return statement, or by throwing an error. Deferred actions are executed in reverse order of how they are specified—that is, the code in the first defer statement executes after code in the second, and so on.
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ErrorHandling.html
 */

//If Else statement
var myInt: Int = 5
if myInt < 3 {
    print("myInt is less than three")
} else if myInt < 10 {
    print("myInt is greater than or equal to three but less than ten")
} else {
    print("myInt is greather than or equal to ten")
}

//Trinary operator for if else
myInt < 3 ? print("myInt is less than three") : print("myInt is greater than or equal to three")

//Function
func foo(num: Int) -> Int {
    return num + 1
}

print(foo(num: 5))

//For Loops
for index in 1...10{
    print(index) //print the numbers 1 through 10
}

let listNames = ["Ben", "Jordan", "Eric", "Eben"]
for (index, value) in listNames.enumerated(){
    print("The name at index \(index) is \(value)") //prints the index and name of each item in list
}

//While Loop
var counter: Int = 1
while counter <= 5{
    print(counter) //print the numbers 1 through 5
    counter += 1
}

//Repeat While Loop (Do-While Loop)
repeat{
    print("I will only be evaluated once!")
} while false

//Swicth Statement
let myChar: Character = "a"
switch myChar{
    case "a":
        print("Option a selected")
    case "b":
        print("Option b selected")
    case "c":
        print("Option c selected")
    default:
        print("Please select a valid option, a, b, or c")
}

//Control Transfer (continue, break, fallthrough, return, throw)
counter = 0
while(true){
    counter += 1
    print(counter)
    if(counter >= 3){
        switch counter{
            case 3:
                print("The number is equal to 3")
                fallthrough
            default:
                print("The number is greater than 0")
        }
        break
    } else {
        continue
    }
}




