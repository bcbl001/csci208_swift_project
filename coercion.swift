/*
 Ben Langley csci208
 Project Math Operators
 Language: Swift
 
 
 swiftc coercion.swift
 ./coercion
 */

/*
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TypeCasting.html
 Keyword is used to determine if a variable is of a certain type
 Keyword as used to downcast (cast to a subclass)
 */

import Foundation

//Implicit coercion: Int -> Double
print(3 + 3.0) //6.0
//print(3 + "String")
print("w" + "String") //wString
//print(1 + true)

//Explicit coercion
print(String(3) + "Swift") //3Swift

var myStr = "3.0"
let myDouble = Double(myStr)
print(myDouble as Any) //Optional(3.0)

print((myStr as NSString).doubleValue) //3.0

print(Int("350") as Any) //Optional(350)

print(("350" as NSString).intValue) //350

print(("3.0" as NSString).intValue) //3

print(("3.5" as NSString).intValue) //3

print(Int(3.5)) //3

print(Int(true)) //1
//print(Int("w"))
//print(Int("hello"))
