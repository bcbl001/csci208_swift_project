/*
 Ben Langley csci208
 Project Implicitly vs. Explicitly Typed
 Language: Swift
 
 
 swiftc implicitExplicitType.swift
 ./implicitExplicitType
 */

/*
 Swift has the capablitily to be implicitly or explicitly typed
 */

//Example of implicitly Typed
var myInt = 9

//Example of explicitly Typed
var myString: String = "Swift"
