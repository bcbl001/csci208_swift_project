/*
 Ben Langley csci208
 Project String Type
 Language: Swift
 
 
 swiftc string.swift
 ./string
 */

/*
 Swift string data type operators found here: https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/StringsAndCharacters.html
 http://useyourloaf.com/blog/swift-string-cheat-sheet/
 \0 is the null character
 */

import Foundation

var str: String = "Swift"


//Math Operators

print(str + str) //SwiftSwift
print("Swift" < "Java") //false
print(str == "Swift") //true
print(str != "Swift") //false
//print(str + 1)
//print(str * 3)
//print(str - str)
//print(str - 2)


//Provided Operators

print("\0") //
print(str.isEmpty) //false
print(str.uppercased()) //SWIFT
print(str.lowercased()) //swift
print(str.characters) //CharacterView(_core: Swift._StringCore(_baseAddress: Optional(0x000000010cf7e890), _countAndFlags: 5, _owner: nil))
str.append("!"); print(str) //Swift!
//var compare = str.caseInsensitiveCompare("swift")
//print(compare)
//"Swift".containsString("S")
//print(str.contains("S"))

str = "Swift"

print(str.characters.count) //5
print(str[str.startIndex]) //S
//print(str.insert("fffff", at: 3))
//print(str.remove(str.index(before: str.endIndex)))
print(str.hasPrefix("Swi")) //true
print(str.hasSuffix("ft")) //true
var charUpTo = str.characters.index(of: "i")!; print(String(str.characters.prefix(upTo: charUpTo))) //Sw


