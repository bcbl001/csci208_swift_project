/*
 Ben Langley csci208
 Project Anonymous Functions (closure expressions), Higher Order Functions, First Class Functions
 Language: Swift
 
 
 swiftc anonymousFunctions.swift
 ./anonymousFunctions
 */

/*
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Closures.html
 
 To apply to an unnammed variable it must be put through an apply function (higher order function). Since clousres are expressions they can be storred to variables (first class functions)
 */


//Example of Map using closure
var ints: [Int] = [1,2,3,4,5,6,7,8,9,10]
ints = ints.map({
    (num: Int) -> Int in
    num * num
})
print(ints) //[1,4,9,16,25,36,49,64,81,100]


//Example of Filter using closure
ints = [1,2,3,4,5,6,7,8,9,10]
ints = ints.filter({
    (num: Int) -> Bool in
    num % 2 == 0
})
print(ints) //[2,4,6,8,10]


//apply the function to value
func apply(value: Int, function: (Int) -> Int) -> Int {
    return function(value)
}

print(apply(value: 5, function: {(num: Int) -> Int in num + 1})) //6

//First class functions
let myFunc: (Int) -> Int = {(num: Int) -> Int in num + 1}
//let myOtherFunc = func foo() {
//    print("hey")
//}
