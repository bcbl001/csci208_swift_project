/*
 Ben Langley csci208
 Project Math Operators
 Language: Swift
 
 
 swiftc mathOperators.swift
 ./mathOperators
 */

/*
 Since Swift is strongly typed it does not allow any nonsense on its math operators. Infact, besides String (not even character) concatonation, math operators only work on numerical variables of the same type. An integer + a float would not work! The comparison operators all work however which yields an interesting result since the math operators do not, yet one string can be "less than" another
 */

var myInt1: Int = 2
var myInt2: Int = 4
var myFloat1: Float = 2.0
var myFloat2: Float = 4.0
var myStr1: String = "hello"
var myStr2: String = "world"
var myChar1: Character = "d"
var myChar2: Character = "w"

//+, -, /, *, %, 
//print(myInt1 + myFloat1)
//print(myFloat2 - myInt2)
print(myFloat2 / myFloat1) //2.0
//print(myFloat2 / myInt2)

print(myStr1 + myStr2) //helloworld
//print(myStr2 - myStr1)
//print(myStr1 / myStr2)
//print(myStr2 * myStr1)
//print(myStr2 * 2)
//print(myStr1 % myStr2)

//print(myChar2 + myChar1)
//print(myChar1 - myChar2)
//print(myChar2 * myChar1)
//print(myChar1 * 3)
//print(myChar1 / myChar2)
//print(myChar2 % myChar1)



//>, <, !=
print(myInt2 < myInt1) //false
print(myInt2 > myInt1) //true
print(myInt2 != myInt1) //true

print(myFloat2 < myFloat1) //false
print(myFloat2 > myFloat1) //true
print(myFloat2 != myFloat1) //true

print(myStr2 < myStr1) //false
print(myStr2 > myStr1) //true
print(myStr2 != myStr1) //true

print(myChar2 < myChar1) //false
print(myChar2 > myChar1) //true
print(myChar2 != myChar1) //true


