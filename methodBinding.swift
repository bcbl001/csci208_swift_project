/*
 Ben Langley csci208
 Project Static vs. Dynamic Method Binding
 Language: Swift
 
 
 swiftc methodBinding.swift
 ./methodBinding
 */

/*
 Swift method binds very similar to Java. When calling a method on an instance of an object dynamic method binding is used. However when used in overloaded functions static method binding is used
 */

class Cloud {
    var name: String
    var altitude: Int
    
    init(name: String, altitude: Int){
        self.name = name
        self.altitude = altitude
    }
    
    func rain(){
        print("It's raining!")
        print("Static method binding")
    }
    
    func toString() -> String{
        return "The clouds name is \(name)"
    }
}

class StormCloud: Cloud{
    override init(name: String, altitude: Int){
        super.init(name: name, altitude: altitude)
    }
    
    override func rain(){
        print("HUGE STORM!!")
        print("Dynamic method binding")
    }
}

func rain(cloud: StormCloud){
    print(cloud.toString())
    print("Dynamic method binding")
}

func rain(cloud: Cloud){
    print(cloud.toString())
    print("Static method binding")
}



let bob: Cloud = StormCloud(name: "Bob", altitude: 6000)
bob.rain() //prints HUGE STROM!! Dyanmic method binding

rain(cloud: bob) //prints The clouds name is bob Static method binding
